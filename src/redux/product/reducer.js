import {
    PRODUCTO,
    PRODUCTO_SUCCESS,
    PRODUCTO_ERROR
  } from '../actions'
  
  const INIT_STATE = {
    productos: [],
    loading: false,
    error: '',
    success: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRODUCTO:
            return Object.assign({ }, state, { loading: true, success: false, error: '' })
        case PRODUCTO_SUCCESS:
            /*let aux = state.productos.filter(item=>{
                return item.id !== action.payload.productos.id
            })
            aux.push(action.payload.productos)*/
            return Object.assign({ }, state, {
                productos: action.payload.productos, 
                loading: false, 
                success: action.payload.message, error: '' 
            })
        case PRODUCTO_ERROR:
            return Object.assign({ }, state, { loading: false, success: false, error: action.payload.message })
        default:
            return state
    }
}