import { all, call, fork, put, takeEvery } from 'redux-saga/effects'

import {
  PRODUCTO,
  apiProducto,
  apiProductoSuccess
} from '../actions'

import {
  withloadAsync,
  withPostPutAsync,
  withDeleteAsync
} from '../api'

//export const URI_REST_PRODUCTOS = 

export function* watchProductos() {
  yield takeEvery(PRODUCTO, withProductos)
}

function* withProductos({ payload }) {
  try{
    const { query, method } = payload
    let _result = null
    switch(method) {
        case 'GET':
            _result = yield call(withloadAsync, '', "api/productos")
            let arr = []
            for(let key in _result){
              arr.push(_result[key])
            }
            yield put(apiProductoSuccess(arr,'EXITO'))
        break;
        case 'DELETE':
          yield call(withDeleteAsync, query, "api/producto")
          yield put(apiProducto('', 'GET'))
          alert('Eliminado Correctamente')
          break;
        default:
          yield call(withPostPutAsync, query, method, "api/producto"+(method==='PUT'?'/'+query.id:''))
          yield put(apiProducto('', 'GET'))
          if(method==='PUT'){
            alert('Actualizado Correctamente')
          }else{
            alert('Creado Correctamente')
          }
        break;
    }
  }catch(error){
    alert('Error intente de nuevo')
      console.log(error)
    //yield put(apiProductoError(parseError(error)))
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchProductos)
  ]);
}