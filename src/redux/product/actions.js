export const PRODUCTO = 'PRODUCTO'
export const PRODUCTO_SUCCESS = 'PRODUCTO_SUCCESS'
export const PRODUCTO_ERROR = 'PRODUCTO_ERROR'

export const apiProducto = (query, method) =>({
    type: PRODUCTO,
    payload: { query, method }
})
  
export const apiProductoSuccess = (productos, message) => ({
  type: PRODUCTO_SUCCESS,
  payload: { productos, message }
})
  
export const apiProductoError = (message) => ({
  type: PRODUCTO_ERROR,
  payload: { message }
})