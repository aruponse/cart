import { all } from 'redux-saga/effects'
import productoSaga from './product/saga'
import cartSaga from './cart/saga'
export default function* rootSaga(getState) {
  yield all(
    [
      productoSaga(),
      cartSaga()
    ]
  )
}