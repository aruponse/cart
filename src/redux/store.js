import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'
import reducers from './reducers'
import storage from 'localforage'
import { persistStore, persistReducer } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import thunk from 'redux-thunk'

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];
const persistConfig = {
  blacklist: ['navigation'],
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2,
  timeout: null
}
const pReducer = persistReducer(persistConfig, reducers)
const store = createStore(
  pReducer,
  applyMiddleware(thunk,...middlewares)
)
const persistor = persistStore(store)

export { store, persistor, sagaMiddleware }