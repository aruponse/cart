export const API_BASE_URL = 'http://localhost:8080/'

export const callApi = (_endpoint, _request)=>{
    return fetch(API_BASE_URL.concat(_endpoint), _request)
        .then(response => {
        const contentType = response.headers.get("content-type")
        if (contentType && contentType.indexOf("application/json") !== -1) {
            return response.json().then(json => {
                if (!response.ok && response.status !== 406) {
                    return Promise.reject(json)
                }
                return Object.assign({}, json)
            })
        } else {
            if(response.ok){
                return response.text().then(text => {
                    return Object.assign({}, { responseText: text })
                });
            }else{
                throw new Error(`${response.status} - ${response.statusText}`)
            }
        } 
    })
  }

  export const withloadAsync = async (query, uri) =>
  await callApi(uri.concat(query), { 
        headers: { 
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })

export const withPostPutAsync = async (data, method, uri) =>
  await callApi(uri,{
      method: method,
      body: JSON.stringify(data),
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
  })

  export const withDeleteAsync = async (id, uri) =>
  await callApi(uri.concat(id),{
      method: 'DELETE',
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
  })