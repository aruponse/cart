import { combineReducers } from 'redux'
import producto from './product/reducer'
import carrito from './cart/reducer'
const appReducer = combineReducers({ 
  producto,
  carrito
})

const reducers = (state, action) => {
  //console.log(state, action)
  return appReducer(state, action)
}

export default reducers;
