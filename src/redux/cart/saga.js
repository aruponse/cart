import { all, call, fork, put, takeEvery } from 'redux-saga/effects'

import {
  GUARDAR,
  LOAD_REALIZADOS,
  loadRealizadosSuccess,
  guardarCart,
  apiCartSuccess
} from '../actions'

import {
  withloadAsync,
  withPostPutAsync,
  withDeleteAsync
} from '../api'

import {guid} from '../../helper/Util'

export function* watchCart() {
  yield takeEvery(GUARDAR, withCart)
}

function* withCart({ payload }) {
  try{
    const { cart, detalle, method } = payload
    let _result = null
    switch(method) {
        case 'GET':
          _result = yield call(withloadAsync, '', "api/cart")
          let _cart = {
            id: _result.id,
            total: _result.total
          }
          _result = yield call(withloadAsync, '', "/api/cartDetails/"+_result.id)
          let cantidad = 0
          let productos = []

          //console.log(_result)
          if(_result && _result.responseText !== ""){
            for(let key in _result){
              cantidad += parseFloat(_result[key].cantidad)
              productos.push({
                id: _result[key].producto.id,
                nombre: _result[key].producto.nombre,
                sku: _result[key].producto.sku,
                descripcion: _result[key].producto.descripcion,
                precio: _result[key].producto.precio,
                tipoProducto: _result[key].producto.tipoProducto,
                cantidad: _result[key].cantidad,
                total: _result[key].cantidad*_result[key].precio,
                detailsId: _result[key].id
              })
            }
          }
          _cart = Object.assign({}, _cart, {
            cantidad: cantidad,
            productos: productos
          })

          yield put(apiCartSuccess(_cart))

          //console.log(_cart)

        break;
        default:
          //console.log(cart, method)
          if(cart.estado==='C'){
            _result = yield call(withloadAsync, '/'+cart.id, "api/checkout")
            yield put(apiCartSuccess({
              productos: [],
              total: 0,
              cantidad: 0
            }))
          }else{
            _result = yield call(withPostPutAsync, cart, method, "api/cart"+(method==='PUT'?'/'+cart.id:''))
            let id = _result && _result.id?_result.id:cart.id
            for(let key in detalle){
              if(detalle[key].cantidad === 0){
                if(detalle[key].detailsId){
                  yield call(withDeleteAsync, '/'+detalle[key].detailsId, "api/cartDetail")
                }
              }else{
                _result = yield call(withPostPutAsync, {
                  id: detalle[key].detailsId? detalle[key].detailsId: guid(),
                  cartId: id,
                  productoId: detalle[key].id,
                  cantidad: detalle[key].cantidad,
                  precio: detalle[key].precio
                }, detalle[key].detailsId?'PUT':'POST', "api/cartDetail"+(detalle[key].detailsId?'/'+detalle[key].detailsId:''))
              }
            }
            yield put(guardarCart(false))
          }
          if(method==='PUT'){
            alert('Actualizado Correctamente')
          }else{
            alert('Creado Correctamente')
          }
        break;
    }
  }catch(error){
    alert('Error intente de nuevo')
      console.log(error)
    //yield put(apiProductoError(parseError(error)))
  }
}


export function* watchRealizados() {
  yield takeEvery(LOAD_REALIZADOS, withRealizados)
}

function* withRealizados( ) {
  try{
    let _result = yield call(withloadAsync,'',"api/carts")
    //console.log(_result)
    let arr =[]
    for(let key in _result){
      arr.push(_result[key])
    }
    //console.log(arr)
    yield put(loadRealizadosSuccess(arr))
  }catch(error){
    console.log(error)
  }
}
export default function* rootSaga() {
  yield all([
    fork(watchCart),
    fork(watchRealizados)
  ]);
}