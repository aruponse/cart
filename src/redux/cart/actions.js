export const CART_SUCCESS = 'CART_SUCCESS'
export const ADD = 'ADD'
export const REMOVE = 'REMOVE'
export const GUARDAR = 'GUARDAR'

export const LOAD_REALIZADOS = 'LOAD_REALIZADOS'
export const LOAD_REALIZADOS_SUCCESS = 'LOAD_REALIZADOS_SUCCESS'

export const guardarCart = (estado, id) =>(dispatch, getState)=>{
  if(typeof estado === 'string'){
    let method= getState().carrito.cart.id?'PUT':'POST'
    let cart = {
      id: id,
      total: getState().carrito.cart.total,
      estado: estado
    }
    let detalle = getState().carrito.cart.productos
    dispatch({
      type: GUARDAR,
      payload: { cart, detalle, method }
    })
  }else{
    let cart={}
    let detalle={}
    let method='GET'
    dispatch({
      type: GUARDAR,
      payload: { cart, detalle, method }
    })
  }
}

export const agregate = (product, edit=false) =>(dispatch, getState)=>{
  let p = getState().carrito.cart.productos.filter(item=>{
    return item.id === product.id
  })
  let productos = getState().carrito.cart.productos.filter(item=>{
    return item.id !== product.id
  })
  if(p.length > 0 && edit === false){
    p = Object.assign({}, p[0], 
      { 
        cantidad: (product.cantidad + p[0].cantidad), 
        total: (parseFloat(product.cantidad + p[0].cantidad) * parseFloat(product.precio)) 
      })
  }else{
    p=product
  }
  productos.push(p)
  let cart = {
    id: getState().carrito.cart.id,
    productos,
    total: productos.reduce((a, b) => a + b.total, 0),
    cantidad: productos.reduce((a, b) => a + b.cantidad, 0)
  }
  //console.log(cart)
  dispatch({
    type: ADD,
    payload: { cart }
  })
}

export const apiCartSuccess = (cart) => ({
  type: CART_SUCCESS,
  payload: { cart }
})

export const loadRealizados = () => ({
  type: LOAD_REALIZADOS
})

export const loadRealizadosSuccess = (realizados) => ({
  type: LOAD_REALIZADOS_SUCCESS,
  payload:{realizados}
})