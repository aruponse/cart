import {
    CART_SUCCESS,
    ADD,
    LOAD_REALIZADOS_SUCCESS
  } from '../actions'
  
  const INIT_STATE = {
    cart:{
        productos: [],
        total: 0,
        cantidad: 0
    },
    realizados:[],
    loading: false,
    error: '',
    success: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ADD:
            return Object.assign({ }, state, { cart: action.payload.cart, loading: true, success: false, error: '' })
        case CART_SUCCESS:
            return Object.assign({ }, state, { cart: action.payload.cart, loading: true, success: false, error: '' })
        case LOAD_REALIZADOS_SUCCESS:
            return Object.assign({ }, state, { realizados: action.payload.realizados })
        default:
            return state
    }
}
