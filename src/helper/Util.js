export function guid() {
    const str4 = () =>
      (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1) // tslint:disable-line
    return `${str4() +
      str4()}-${str4()}-${str4()}-${str4()}-${str4()}${str4()}${str4()}`
  }


  export const EnumTipoProducto=[
  {key:'S',value:'S',label:'Simple'},
  {key:'D',value:'D',label:'Con Descuento'}
]