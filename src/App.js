import React from 'react';
//import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import ViewError from './views/Error'
import DefaultPage from './views/DefaultPage'
import Product from './views/Product'
import Comprar from './views/Comprar'
import Cart from './views/Cart'
import Comprados from './views/Comprados'
import './assets/css/App.css'

class App extends React.Component {
  render() {
    return (
      <div className="h-100">
          <>
              <Router>
                <Switch>
                  <Route
                    path="/"
                    exact
                    render={(props) => <DefaultPage {...props} />}
                  />
                  <Route
                    path="/product"
                    exact
                    render={(props) => <Product {...props} />}
                  />
                  <Route
                    path="/comprar"
                    exact
                    render={(props) => <Comprar {...props} />}
                  />
                  <Route
                    path="/carrito"
                    exact
                    render={(props) => <Cart {...props} />}
                  />
                  <Route
                    path="/comprados"
                    exact
                    render={(props) => <Comprados {...props} />}
                  />
                  <Route
                      path="/error"
                      exact
                      render={(props) => <ViewError {...props} />}
                    />
                  <Redirect to="/error" />
                </Switch>
              </Router>
          </>
      </div>
    )
  }
}

export default App;

/*const mapStateToProps = ({  }) => {
  
  return { }
}
const mapActionsToProps = { }

export default connect(mapStateToProps, mapActionsToProps)(App);*/

