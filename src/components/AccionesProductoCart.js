import React from 'react';
import {connect} from 'react-redux'
import {Button, Input} from 'reactstrap'
import {agregate} from '../redux/actions'

const AccionesProductoCart=({item, agregate})=>{
    return(
        <div style={{ flex:1, justifyContent:'space-between', alignItems:'center'}}>
            <Button type="button" onClick={()=>{
                agregate(Object.assign({ }, item, {
                    cantidad: item.cantidad + 1,
                    total: parseFloat(item.cantidad + 1)*parseFloat(item.precio)
                }), true)
            }}>+</Button>
            <Input style={{ width: 30, textAlign:'center' }} value={item.cantidad} onChange={(event)=>{
                agregate(Object.assign({ }, item, {
                    cantidad: parseFloat(event.target.value),
                    total: parseFloat(event.target.value)*parseFloat(item.precio)
                }), true)
            }}/>
            <Button type="button" onClick={()=>{
                if(item.cantidad > 0){
                    agregate(Object.assign({ }, item, {
                        cantidad: item.cantidad - 1,
                        total: parseFloat(item.cantidad - 1)*parseFloat(item.precio)
                    }), true)
                }
            }}>-</Button>
        </div>
    )
}
const mapStateToProps = ({ carrito }) => {
const {cart} = carrito
    return { cart }
}
const mapActionsToProps = { agregate }
export default connect(mapStateToProps, mapActionsToProps)(AccionesProductoCart);