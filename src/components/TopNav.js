import React from 'react';
import {connect}from 'react-redux'
import { NavLink } from 'react-router-dom';
//icono
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'
import { Label } from 'reactstrap';

const TopNav=({cart})=>{
    return (
        <nav className="navbar fixed-top">
            <div className="d-flex align-items-center">
                <NavLink
                    to="#"
                    location={{}}
                    className="m4"
                >
                    Logo
                </NavLink>
                <NavLink
                    to="/product"
                    location={{}}
                    className="m4"
                >
                    Productos
                </NavLink>
                <NavLink
                    to="/comprar"
                    location={{}}
                    className="m4"
                >
                    Comprar
                </NavLink>

                <NavLink
                    to="/comprados"
                    location={{}}
                    className="m4"
                >
                    Compras Realizadas
                </NavLink>

                <NavLink
                    to="/carrito"
                    location={{}}
                    className="m4"
                >
                    <FontAwesomeIcon icon={faCartPlus} />
                    <Label>{cart.cantidad}</Label>
                </NavLink>
            </div>
        </nav>
    )
}

const mapStateToProps = ({ carrito }) => {
const {cart} = carrito
    return { cart }
}
const mapActionsToProps = { }
    
export default connect(mapStateToProps, mapActionsToProps)(TopNav);