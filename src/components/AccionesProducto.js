import React from 'react';
import {connect} from 'react-redux'
import {Button, Input} from 'reactstrap'
import {agregate} from '../redux/actions'

const AccionesProducto=({item, agregate})=>{
    const [cantidad, setCantidad]=React.useState(1)
    return(
        <div style={{ flex:1, justifyContent:'space-between', alignItems:'center'}}>
            <Button type="button" onClick={()=>{
                setCantidad(cantidad+1)
            }}>+</Button>
            <Input style={{ width: 30, textAlign:'center' }} value={cantidad} onChange={(event)=>{
                setCantidad(event.target.value);
            }}/>
            <Button type="button" onClick={()=>{
                if(cantidad > 1){
                    setCantidad(cantidad-1)
                }
            }}>-</Button>
            <Button style={{ marginLeft: 8 }} type="button" onClick={()=>{
                agregate(Object.assign({}, item, {
                    cantidad: cantidad,
                    total: parseFloat(cantidad)*parseFloat(item.precio)
                }))
             }}>ADD</Button>
        </div>
    )
}

    const mapStateToProps = ({ carrito }) => {
    const {cart} = carrito
        return { cart }
    }
    const mapActionsToProps = { agregate }
        
    export default connect(mapStateToProps, mapActionsToProps)(AccionesProducto);
//export default AccionesProducto