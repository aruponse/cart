import React from 'react';
import {connect} from 'react-redux'
import {
    Table,
    Label,
    Input,
    Button
  } from 'reactstrap';
import {apiProducto} from '../redux/actions'
import Select from 'react-select'
import CustomSelectInput from '../components/CustomSelectInput'
import {guid,EnumTipoProducto} from '../helper/Util'
import TopNav from '../components/TopNav'

const Product=({ apiProducto, productos })=>{
    const[id, setId]=React.useState(false)
    const[nombre, setnombre]=React.useState('')
    const[sku,setsku]=React.useState('')
    const[descripcion,setdescripcion]=React.useState('')
    const[precio,setprecio]=React.useState('')
    const[tipo_producto,settipo_producto]=React.useState('')

    React.useEffect(()=>{
        async function load(){
            apiProducto('','GET')
        }
        load()
    },[apiProducto])

    return (
    <>
    <TopNav />
        <h1>Ingreso de Producto</h1>
        <Table>
        <tbody>
            <tr>
                <th style={{ textAlign: 'right'}}>
                    <Label>Nombre:</Label>
                </th>
                <td>
                    <Input 
                    value={nombre}
                    onChange={(event)=>{setnombre(event.target.value)}}/>
                </td>
            </tr>
            <tr>
                <th style={{ textAlign: 'right'}}>
                    <Label>Sku:</Label>
                </th>
                <td>
                    <Input 
                    value={sku}
                    onChange={(event)=>{setsku(event.target.value)}}/>
                </td>
            </tr>
            <tr>
                <th style={{ textAlign: 'right'}}>
                    <Label>Descripción:</Label>
                </th>
                <td>
                    <Input 
                    value={descripcion}
                    onChange={(event)=>{setdescripcion(event.target.value)}}/>
                </td>
            </tr>
            <tr>
                <th style={{ textAlign: 'right'}}>
                    <Label>Precio:</Label>
                </th>
                <td>
                    <Input 
                    value={precio}
                    onChange={(event)=>{setprecio(event.target.value)}}/>
                </td>
            </tr>
            <tr>
                <th style={{ textAlign: 'right'}}>
                    <Label>Tipo Producto:</Label>
                </th>
                <td>
                <Select
                    components={{ Input: CustomSelectInput }}
                    className="react-select"
                    classNamePrefix="react-select"
                    options={EnumTipoProducto}
                    onChange={(e)=>{settipo_producto(e.value)}}
                    value={EnumTipoProducto.filter(item=>{
                        return item.value === tipo_producto
                    })[0]} />
                </td>
            </tr>
            </tbody>
        </Table>
        <Button className="m1" onClick={()=>{
            setId(false)
            setnombre('')
            setsku('')
            setdescripcion('')
            setprecio('')
            settipo_producto('')
        }}>
            Nuevo
        </Button>
        <Button 
        className="m16"
        type="button" onClick={()=>{
            if(id){
                apiProducto({
                    id: id,
                    nombre: nombre,
                    sku: sku,
                    descripcion: descripcion,
                    precio: precio,
                    tipoProducto: tipo_producto
                },'PUT')
            }else{
                let _id = guid()
                setId(_id)
                apiProducto({
                    id: _id,
                    nombre: nombre,
                    sku: sku,
                    descripcion: descripcion,
                    precio: precio,
                    tipoProducto: tipo_producto
                },'POST')
            }
        }}>Guardar</Button>
        <h2>Listado de Productos</h2>
        <Table className="m16 w-100 table-bordered">
        <tbody>
            <tr>
                <th style={{ width: '20%'}}>
                    <Label>Nombre</Label>
                </th>
                <th style={{ width: '20%'}}>
                    <Label>Sku</Label>
                </th>
                <th style={{ width: '20%'}}>
                    <Label>Descripción</Label>
                </th>
                <th style={{ width: '10%'}}>
                    <Label>Precio</Label>
                </th>
                <th style={{ width: '20%'}}>
                    <Label>Tipo Producto</Label>
                </th>
                <th style={{ width: '5%'}}>
                    <Label>Editar</Label>
                </th>
                <th style={{ width: '5%'}}>
                    <Label>Editar</Label>
                </th>
            </tr>
            {
                productos.map((item)=>{
                    return(
                        <tr key={item.id}>
                            <td>
                                <Label>{item.nombre}</Label>
                            </td>
                            <td>
                                <Label>{item.sku}</Label>
                            </td>
                            <td>
                                <Label>{item.descripcion}</Label>
                            </td>
                            <td>
                                <Label>{item.precio}</Label>
                            </td>
                            <td>
                                <Label>
                                    {
                                        EnumTipoProducto.filter(_item=>{
                                            return _item.value === item.tipoProducto
                                        })[0].label
                                    }
                                </Label>
                            </td>
                            <td>
                                <Button type="button" onClick={()=>{
                                    setId(item.id)
                                    setnombre(item.nombre)
                                    setsku(item.sku)
                                    setdescripcion(item.descripcion)
                                    setprecio(item.precio)
                                    settipo_producto(item.tipoProducto)
                                }}>
                                    {'->'}
                                </Button>
                            </td>
                            <td>
                                <Button className="m1" onClick={()=>{
                                    apiProducto(`/${item.id}`,'DELETE')
                                }}>
                                    Eliminar
                                </Button>
                            </td>
                        </tr>
                    )
                })
            }
            </tbody>
        </Table>
    </>)
}

const mapStateToProps = ({ producto }) => {
  const {productos} = producto
    return { productos }
  }
  const mapActionsToProps = { apiProducto }
  
  export default connect(mapStateToProps, mapActionsToProps)(Product);