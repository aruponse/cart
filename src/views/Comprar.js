import React from 'react';
import {connect} from 'react-redux'
import {
    Table,
    Label
  } from 'reactstrap';
import {apiProducto} from '../redux/actions'
import AccionesProducto from '../components/AccionesProducto'
import TopNav from '../components/TopNav'
import {EnumTipoProducto} from '../helper/Util'


const Comprar=({ apiProducto, productos })=>{

    return (
    <>
    <TopNav />
        <h1>Seleccionar Producto</h1>
        <Table className="m16 w-100 table-bordered">
        <tbody>
            <tr>
                <th style={{ width: '20%'}}>
                    <Label>Nombre</Label>
                </th>
                <th style={{ width: '20%'}}>
                    <Label>Sku</Label>
                </th>
                <th style={{ width: '20%'}}>
                    <Label>Descripción</Label>
                </th>
                <th style={{ width: '10%'}}>
                    <Label>Precio</Label>
                </th>
                <th style={{ width: '10%'}}>
                    <Label>Tipo Producto</Label>
                </th>
                <th style={{ width: '20%'}}></th>
            </tr>
            {
                productos.map((item)=>{
                    return(
                        <tr key={item.id}>
                            <td>
                                <Label>{item.nombre}</Label>
                            </td>
                            <td>
                                <Label>{item.sku}</Label>
                            </td>
                            <td>
                                <Label>{item.descripcion}</Label>
                            </td>
                            <td>
                                <Label>{item.precio}</Label>
                            </td>
                            <td>
                                <Label>{
                                        EnumTipoProducto.filter(_item=>{
                                            return _item.value === item.tipoProducto
                                        })[0].label
                                    }</Label>
                            </td>
                            <td>
                                <AccionesProducto item={item} />
                            </td>
                        </tr>
                    )
                })
            }
            </tbody>
        </Table>
    </>)
}

const mapStateToProps = ({ producto }) => {
  const {productos} = producto
    return { productos }
  }
  const mapActionsToProps = { apiProducto }
  
  export default connect(mapStateToProps, mapActionsToProps)(Comprar);