import React from 'react';
import {connect} from 'react-redux'
import {
    Table,
    Button
  } from 'reactstrap';
import {guardarCart} from '../redux/actions'
import TopNav from '../components/TopNav'
import AccionesProductoCart from '../components/AccionesProductoCart'
import {guid} from '../helper/Util'

const Cart=({ cart, guardarCart })=>{
  return(<>
    <TopNav />
    <h1>Orden de Compra</h1>
    <Table className="m16 w-100 table-bordered" >
      <tbody>
        <tr>
          <th style={{ width:'30%' }}>Producto</th>
          <th style={{ width:'10%' }}>Cantidad</th>
          <th style={{ width:'10%' }}>Valor</th>
        </tr>
        {
          cart.productos.sort((a,b)=>{
            return a.nombre.localeCompare(b.nombre)
          }).map((item)=>{
            return(
              <tr key={item.id}>
                <td style={{ textAlign:'left' }}>{item.nombre}</td>
                <td style={{ textAlign:'right' }}><AccionesProductoCart item={item} /></td>
                <td style={{ textAlign:'right' }}>{item.cantidad * item.precio}</td>
              </tr>
            )
          })
        }
        <tr>
          <td colSpan="2">Total</td>
          <td style={{ textAlign:'right' }}>{cart.productos.reduce((a,b)=>a+(b.cantidad*b.precio),0)}</td>
        </tr>
      </tbody>
    </Table>
    <Button type="button" onClick={()=>{
      if(cart.id){
        guardarCart('P', cart.id)
      }else{
        guardarCart('P', guid())
      }
    }}>Guardar</Button>

  <Button style={{ marginLeft: 8 }} type="button" onClick={()=>{
      guardarCart('C', cart.id)
    }}>Pagar</Button>
  </>)
}

const mapStateToProps = ({ carrito }) => {
  const {cart}=carrito
    return { cart}
  }
  const mapActionsToProps = { guardarCart }
  
  export default connect(mapStateToProps, mapActionsToProps)(Cart);