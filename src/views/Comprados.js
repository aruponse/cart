import React from 'react';
import {connect} from 'react-redux'
import {
    Table
  } from 'reactstrap';
import {loadRealizados} from '../redux/actions'
import TopNav from '../components/TopNav'

const Comprados=({ realizados, loadRealizados })=>{

    React.useEffect(()=>{
        async function load(){
            loadRealizados('','GET')
        }
        load()
    },[loadRealizados])

  return(
    <>
        <TopNav />
        <h1>Compras Realizadas</h1>
        <Table className="m16 w-100 table-bordered">
            <tbody>
                <tr>
                    <th style={{ width:'20%'}}>Id</th>
                    <th style={{ width:'60%'}}>Total</th>
                    <th style={{ width:'20%'}}>Estado</th>
                </tr>
                {
                    realizados.map((item)=>{
                        return(
                        <tr key={item.id}>
                            <td>{item.id}</td>
                            <td style={{ textAlign:'right'}}>$ {item.total}</td>
                            <td>{item.estado==='P'?'Pendiente':'Complato'}</td>
                        </tr>
                        )
                    })
                }
            </tbody>      
        </Table>
    </>)
}

const mapStateToProps = ({ carrito }) => {
  const {realizados}=carrito
    return { realizados}
  }
  const mapActionsToProps = { loadRealizados }
  
  export default connect(mapStateToProps, mapActionsToProps)(Comprados);